def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board, index):
    print_board(board)
    print("GAME OVER")
    print(board[index], "has won")
    exit()


def is_row_winner(board, pos):
    return board[pos] == board[pos - 1] and board[pos] == board[pos + 1]


def is_column_winner(board, pos):
    return board[pos] == board[pos - 3] and board[pos] == board[pos + 3]


def is_diagonal_winner(board, pos):
    return (board[pos] == board[pos - 4] and board[pos] == board[pos + 4]) or (board[pos] == board[pos - 2] and board[pos] == board[pos + 2])



board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, 1)
    elif is_row_winner(board, 4):
        game_over(board, 4)
    elif is_row_winner(board, 7):
        game_over(board, 7)
    elif is_column_winner(board, 3):
        game_over(board, 3)
    elif is_column_winner(board, 4):
        game_over(board, 4)
    elif is_column_winner(board, 5):
        game_over(board, 5)
    elif is_diagonal_winner(board, 4):
        game_over(board, 4)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
